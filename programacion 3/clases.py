from ctypes.wintypes import PINT


class Mesa : #defino clase. Clase: es un conjunto de caracteristicas de algun objeto fisico o no.Es un modelo con un conjunto de atributos y metoos
    #ATRIBUTOS:  puede ser int o string. Caracteristica de la clase
    precio = 0  #
    forma = ""  # inicializar
    tamaño = "" #
    
    
    #CONSTRUCTOR = inicializar los atributos 
    def __init__(self, precio, forma, tamaño):
        #atributos || parametros :valor de atributo || slf sirve para llamar a los atributos y metodos
        self.precio = precio
        self.forma = forma
        self.tamaño = tamaño
    
    #Metodo: son funciones para las clases
    def mostrar_datos(self): #Definir un metodo
        print("La mesa de " + str(self.precio) + " tiene forma " + self.forma + " y tiene un tamaño " + self.tamaño)
    
#Objeto(variable) que ya tiene definida sus carateristicas. Cada objeto es distinto || Instanciar (crear) un objeto. 
mesa1= Mesa(1000, "cuadrado", "grande")
mesa2= Mesa(500, "cicular", "pequeña")

mesa1.mostrar_datos() #invocamos un metodo: Lo llamamos
mesa2.mostrar_datos()